use logos::{Lexer, Logos, Source};
use std::fmt::{Display, Formatter};

/// Tuple struct for link URLs
#[derive(Debug, PartialEq)]
pub struct LinkUrl(String);

/// Implement Display for printing
impl Display for LinkUrl {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Tuple struct for link texts
#[derive(Debug, PartialEq)]
pub struct LinkText(String);

/// Implement Display for printing
impl Display for LinkText {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Token enum for capturing of link URLs and Texts
#[derive(Logos, Debug, PartialEq)]
pub enum URLToken {
	#[regex("<a([^\">h]|h[^\">r]|hr[^\">e]|hre[^\">f])*(\"[^\"]*\"([^\">h]|h[^\">r]|hr[^\">e]|hre[^\">f])*)*href[^\">]*(\"[^\"]*\"[^\">]*)*>[^<]*<[ \t\n\r]*/[ \t\n\r]*a[ \t\n\r]*>", extract_link_info)]
    Link((LinkUrl, LinkText)),

    // TODO: Ignore all characters that do not belong to a link definition
	#[regex("[^<]|<[^a]|<a([^\">h]|h[^\">r]|hr[^\">e]|hre[^\">f])*(\"[^\"]*\"([^\">h]|h[^\">r]|hr[^\">e]|hre[^\">f])*)*>", logos::skip)]
    Ignored,
    // Catch any error
    #[error]
    Error,
}

/// Extracts the URL and text from a string that matched a Link token
fn extract_link_info(lex: &mut Lexer<URLToken>) -> (LinkUrl, LinkText) {
    let slice: &str = lex.slice();
	let replacedSlice: String = slice.replace("[ \t\n\r]*", "");
	let mut parentheses = 0;
	let mut pos : usize = 0;
	for (i, chr) in slice.bytes().enumerate() {
		if chr == '\"' as u8 {
			parentheses += 1;
		} else if parentheses%2 == 0 {
			if slice[i..i+6] == "href=\"".to_string() {
				pos = i+6;
				break;
			}
		}
	}
	let url = replacedSlice[pos..].split_once("\"").unwrap().0;
	
	let mut posStart : usize = 0;
	parentheses = 0;
	for (i, chr) in slice.bytes().enumerate() {
		if chr == '\"' as u8 {
			parentheses += 1;
		} else if parentheses%2 == 0 {
			if chr == '>' as u8 {
				posStart = i+1;
				break;
			}
		}
	}
	let mut posEnd : usize = 0;
	for (i, chr) in slice.bytes().enumerate() {
		if chr == '<' as u8 {
			posEnd = i;
		}
	}

    // TODO: Implement extraction from link definition
    (LinkUrl(url.to_string()), LinkText(slice[posStart..posEnd].to_string()))
}
